//In JS, classes can be created by using the "class" keyword and {}.
//Naming convention ofr classes: begin with uppercase letters

/*
	Syntax:
	class <Name>{
	}
*/

//Student class
class Student {
	//to enable students instantiated from this class to have distinct names and emails
	//our constructor must be able to accept name and email arguments
	//which it will then use to set the value of the object's corresponding properties
	constructor(name,email,grades){
		this.name = name;
		this.email = email;
	}
	//class methods
		//these are common to all instances
	
 
    computeAve() {
        let sum = 0;
        this.grades.forEach(grade => {sum = sum + grade;})
        return sum/4;
      }
      willPass() {
        this.passed = this.computeAve().gradeAve >= 85;
        return this;
      }
      willPassWithHonors() {
        if (this.passed) {
          this.passedWithHonors = this.gradeAve >= 90;
        } else {
          this.passedWithHonors = false;
        }
        return this;
      }
      login(){
		console.log(`${this.email} has logged in`);
		return this;
	}
    logout(){
        console.log(`${this.email} has logged out`)
    }
    listGrades(){
        console.log(`${this.name}'s quarterly grade averages are: ${this.grade}`)
    }
}
//instances
let studentOne = new Student('john','john@mail.com');
let studentTwo = new Student('jane','jane@mail.com');
/* The login() method logs a message to the console indicating that the student with the specific email has logged in.

The logout() method logs a message to the console indicating that the student with the specific email has logged out.
The listGrades() method logs a message to the console displaying the name of the student and their quarterly grade averages.
The computeAve() method calculates the average of the student's grades by iterating over the this.grades array and summing up all the grades.
The result is stored in the this.gradeAve property.

The willPass() method determines if the student passed or failed based on their average grade. It calls the computeAve() method to calculate the average grade and then checks if the gradeAve property is greater than or equal to 85. The result is stored in the this.passed property.

The willPassWithHonors() method determines if the student passed with honors based on their average grade. It first checks if the student passed (this.passed is true) and then checks if the gradeAve property is greater than or equal to 90. The result is stored in the this.passedWithHonors property.

/* 
let studentOne = new Student('John', 'john@mail.com', [89, 84, 78, 88]);
let studentTwo = new Student('Joe', 'joe@mail.com', [78, 82, 79, 85]);
let studentThree = new Student('Jane', 'jane@mail.com', [87, 89, 91, 93]);
let studentFour = new Student('Jessie', 'jessie@mail.com', [91, 89, 92, 93]);
*/