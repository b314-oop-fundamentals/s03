class Student {
	constructor(name,email,grades){
		this.name = name;
		this.email = email;
	}
    computeAve() {
        let sum = 0;
        this.grades.forEach(grade => {sum = sum + grade;})
        return sum/4;
      }
    willPass() {
        this.passed = this.computeAve().gradeAve >= 85;
        return this;
      }
    willPassWithHonors() {
        if (this.passed) {
          this.passedWithHonors = this.gradeAve >= 90;
        } else {
          this.passedWithHonors = false;
        }
        return this;
      }
    login(){
		console.log(`${this.email} has logged in`);
		return this;
	}
    logout(){
        console.log(`${this.email} has logged out`)
    }
    listGrades(){
        console.log(`${this.name}'s quarterly grade averages are: ${this.grade}`)
    }
}
/*
1) What is the blueprint where objects are created from?
Answer: constructor
2) What is the naming convention applied to classes?    PascalCase
3) What keyword do we use to create objects from a class? new
4) What is the technical term for creating an object from a class? Instantiation
5) What class method dictates HOW objects will be created from that class?  constructor method */